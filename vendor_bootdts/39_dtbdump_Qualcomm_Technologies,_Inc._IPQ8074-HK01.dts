/dts-v1/;

/ {
	#address-cells = <0x02>;
	#size-cells = <0x02>;
	compatible = "qcom,ipq8074-hk01\0qcom,ipq8074";
	interrupt-parent = <0x01>;
	model = "Qualcomm Technologies, Inc. IPQ8074-HK01";

	__symbols__ {
		CPU0 = "/cpus/cpu@0";
		CPU1 = "/cpus/cpu@1";
		CPU2 = "/cpus/cpu@2";
		CPU3 = "/cpus/cpu@3";
		L2_0 = "/cpus/l2-cache";
		blsp1_i2c2 = "/soc/i2c@78b6000";
		blsp1_i2c3 = "/soc/i2c@78b7000";
		blsp1_spi1 = "/soc/spi@78b5000";
		blsp1_uart1 = "/soc/serial@78af000";
		blsp1_uart3 = "/soc/serial@78b1000";
		blsp1_uart5 = "/soc/serial@78b3000";
		blsp_dma = "/soc/dma@7884000";
		gcc = "/soc/gcc@1800000";
		hsuart_pins = "/soc/pinctrl@1000000/hsuart-pins";
		i2c_0_pins = "/soc/pinctrl@1000000/i2c-0-pinmux";
		intc = "/soc/interrupt-controller@b000000";
		pcie0 = "/soc/pci@20000000";
		pcie1 = "/soc/pci@10000000";
		pcie_phy0 = "/soc/phy@86000";
		pcie_phy1 = "/soc/phy@8e000";
		qpic_bam = "/soc/dma@7984000";
		qpic_nand = "/soc/nand@79b0000";
		qpic_pins = "/soc/pinctrl@1000000/qpic-pins";
		serial_4_pins = "/soc/pinctrl@1000000/serial4-pinmux";
		sleep_clk = "/clocks/sleep_clk";
		soc = "/soc";
		spi_0_pins = "/soc/pinctrl@1000000/spi-0-pins";
		tlmm = "/soc/pinctrl@1000000";
		xo = "/clocks/xo";
	};

	aliases {
		serial0 = "/soc/serial@78b3000";
		serial1 = "/soc/serial@78b1000";
	};

	chosen {
		stdout-path = "serial0";
	};

	clocks {

		sleep_clk {
			#clock-cells = <0x00>;
			clock-frequency = <0x7d00>;
			compatible = "fixed-clock";
			linux,phandle = <0x1c>;
			phandle = <0x1c>;
		};

		xo {
			#clock-cells = <0x00>;
			clock-frequency = <0x124f800>;
			compatible = "fixed-clock";
			linux,phandle = <0x1d>;
			phandle = <0x1d>;
		};
	};

	cpus {
		#address-cells = <0x01>;
		#size-cells = <0x00>;

		cpu@0 {
			compatible = "arm,cortex-a53";
			device_type = "cpu";
			enable-method = "psci";
			linux,phandle = <0x18>;
			next-level-cache = <0x0d>;
			phandle = <0x18>;
			reg = <0x00>;
		};

		cpu@1 {
			compatible = "arm,cortex-a53";
			device_type = "cpu";
			enable-method = "psci";
			linux,phandle = <0x19>;
			next-level-cache = <0x0d>;
			phandle = <0x19>;
			reg = <0x01>;
		};

		cpu@2 {
			compatible = "arm,cortex-a53";
			device_type = "cpu";
			enable-method = "psci";
			linux,phandle = <0x1a>;
			next-level-cache = <0x0d>;
			phandle = <0x1a>;
			reg = <0x02>;
		};

		cpu@3 {
			compatible = "arm,cortex-a53";
			device_type = "cpu";
			enable-method = "psci";
			linux,phandle = <0x1b>;
			next-level-cache = <0x0d>;
			phandle = <0x1b>;
			reg = <0x03>;
		};

		l2-cache {
			cache-level = <0x02>;
			compatible = "cache";
			linux,phandle = <0x0d>;
			phandle = <0x0d>;
		};
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0x00 0x40000000 0x00 0x20000000>;
	};

	pmu {
		compatible = "arm,armv8-pmuv3";
		interrupts = <0x01 0x07 0xf04>;
	};

	psci {
		compatible = "arm,psci-1.0";
		method = "smc";
	};

	soc {
		#address-cells = <0x01>;
		#size-cells = <0x01>;
		compatible = "simple-bus";
		linux,phandle = <0x0e>;
		phandle = <0x0e>;
		ranges = <0x00 0x00 0x00 0xffffffff>;

		dma@7884000 {
			#dma-cells = <0x01>;
			clock-names = "bam_clk";
			clocks = <0x02 0x15>;
			compatible = "qcom,bam-v1.7.0";
			interrupts = <0x00 0xee 0x04>;
			linux,phandle = <0x04>;
			phandle = <0x04>;
			qcom,ee = <0x00>;
			reg = <0x7884000 0x2b000>;
		};

		dma@7984000 {
			#dma-cells = <0x01>;
			clock-names = "bam_clk";
			clocks = <0x02 0x29>;
			compatible = "qcom,bam-v1.7.0";
			interrupts = <0x00 0x92 0x04>;
			linux,phandle = <0x08>;
			phandle = <0x08>;
			qcom,ee = <0x00>;
			reg = <0x7984000 0x1a000>;
			status = "ok";
		};

		gcc@1800000 {
			#clock-cells = <0x01>;
			#reset-cells = <0x01>;
			compatible = "qcom,gcc-ipq8074";
			linux,phandle = <0x02>;
			phandle = <0x02>;
			reg = <0x1800000 0x80000>;
		};

		i2c@78b6000 {
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			clock-frequency = <0x61a80>;
			clock-names = "iface\0core";
			clocks = <0x02 0x15 0x02 0x18>;
			compatible = "qcom,i2c-qup-v2.2.1";
			dma-names = "rx\0tx";
			dmas = <0x04 0x0f 0x04 0x0e>;
			interrupts = <0x00 0x60 0x04>;
			linux,phandle = <0x13>;
			phandle = <0x13>;
			pinctrl-0 = <0x07>;
			pinctrl-names = "default";
			reg = <0x78b6000 0x600>;
			status = "ok";
		};

		i2c@78b7000 {
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			clock-frequency = <0x186a0>;
			clock-names = "iface\0core";
			clocks = <0x02 0x15 0x02 0x1a>;
			compatible = "qcom,i2c-qup-v2.2.1";
			dma-names = "rx\0tx";
			dmas = <0x04 0x11 0x04 0x10>;
			interrupts = <0x00 0x61 0x04>;
			linux,phandle = <0x14>;
			phandle = <0x14>;
			reg = <0x78b7000 0x600>;
			status = "disabled";
		};

		interrupt-controller@b000000 {
			#interrupt-cells = <0x03>;
			compatible = "qcom,msm-qgic2";
			interrupt-controller;
			linux,phandle = <0x01>;
			phandle = <0x01>;
			reg = <0xb000000 0x1000 0xb002000 0x1000>;
		};

		nand@79b0000 {
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			clock-names = "core\0aon";
			clocks = <0x02 0x2a 0x02 0x29>;
			compatible = "qcom,ipq8074-nand";
			dma-names = "tx\0rx\0cmd";
			dmas = <0x08 0x00 0x08 0x01 0x08 0x02>;
			linux,phandle = <0x15>;
			phandle = <0x15>;
			pinctrl-0 = <0x09>;
			pinctrl-names = "default";
			reg = <0x79b0000 0x10000>;
			status = "ok";

			nand@0 {
				nand-bus-width = <0x08>;
				nand-ecc-step-size = <0x200>;
				nand-ecc-strength = <0x04>;
				reg = <0x00>;
			};
		};

		pci@10000000 {
			#address-cells = <0x03>;
			#interrupt-cells = <0x01>;
			#size-cells = <0x02>;
			bus-range = <0x00 0xff>;
			clock-names = "iface\0axi_m\0axi_s\0ahb\0aux";
			clocks = <0x02 0x7a 0x02 0x77 0x02 0x78 0x02 0x75 0x02 0x76>;
			compatible = "qcom,pcie-ipq8074";
			device_type = "pci";
			interrupt-map = <0x00 0x00 0x00 0x01 0x01 0x00 0x8e 0x04 0x00 0x00 0x00 0x02 0x01 0x00 0x8f 0x04 0x00 0x00 0x00 0x03 0x01 0x00 0x90 0x04 0x00 0x00 0x00 0x04 0x01 0x00 0x91 0x04>;
			interrupt-map-mask = <0x00 0x00 0x00 0x07>;
			interrupt-names = "msi";
			interrupts = <0x00 0x55 0x04>;
			linux,pci-domain = <0x01>;
			linux,phandle = <0x17>;
			num-lanes = <0x01>;
			perst-gpio = <0x0b 0x3d 0x01>;
			phandle = <0x17>;
			phy-names = "pciephy";
			phys = <0x0c>;
			ranges = <0x81000000 0x00 0x10200000 0x10200000 0x00 0x100000 0x82000000 0x00 0x10300000 0x10300000 0x00 0xd00000>;
			reg = <0x10000000 0xf1d 0x10000f20 0xa8 0x88000 0x2000 0x10100000 0x1000>;
			reg-names = "dbi\0elbi\0parf\0config";
			reset-names = "pipe\0sleep\0sticky\0axi_m\0axi_s\0ahb\0axi_m_sticky";
			resets = <0x02 0x7c 0x02 0x7d 0x02 0x7e 0x02 0x7f 0x02 0x80 0x02 0x81 0x02 0x82>;
			status = "ok";
		};

		pci@20000000 {
			#address-cells = <0x03>;
			#interrupt-cells = <0x01>;
			#size-cells = <0x02>;
			bus-range = <0x00 0xff>;
			clock-names = "iface\0axi_m\0axi_s\0ahb\0aux";
			clocks = <0x02 0x74 0x02 0x71 0x02 0x72 0x02 0x6f 0x02 0x70>;
			compatible = "qcom,pcie-ipq8074";
			device_type = "pci";
			interrupt-map = <0x00 0x00 0x00 0x01 0x01 0x00 0x4b 0x04 0x00 0x00 0x00 0x02 0x01 0x00 0x4e 0x04 0x00 0x00 0x00 0x03 0x01 0x00 0x4f 0x04 0x00 0x00 0x00 0x04 0x01 0x00 0x53 0x04>;
			interrupt-map-mask = <0x00 0x00 0x00 0x07>;
			interrupt-names = "msi";
			interrupts = <0x00 0x34 0x04>;
			linux,pci-domain = <0x00>;
			linux,phandle = <0x16>;
			num-lanes = <0x01>;
			perst-gpio = <0x0b 0x3a 0x01>;
			phandle = <0x16>;
			phy-names = "pciephy";
			phys = <0x0a>;
			ranges = <0x81000000 0x00 0x20200000 0x20200000 0x00 0x100000 0x82000000 0x00 0x20300000 0x20300000 0x00 0xd00000>;
			reg = <0x20000000 0xf1d 0x20000f20 0xa8 0x80000 0x2000 0x20100000 0x1000>;
			reg-names = "dbi\0elbi\0parf\0config";
			reset-names = "pipe\0sleep\0sticky\0axi_m\0axi_s\0ahb\0axi_m_sticky";
			resets = <0x02 0x75 0x02 0x76 0x02 0x77 0x02 0x78 0x02 0x79 0x02 0x7a 0x02 0x7b>;
			status = "ok";
		};

		phy@86000 {
			#phy-cells = <0x00>;
			clock-names = "pipe_clk";
			clock-output-names = "pcie20_phy0_pipe_clk";
			clocks = <0x02 0x73>;
			compatible = "qcom,ipq8074-qmp-pcie-phy";
			linux,phandle = <0x0a>;
			phandle = <0x0a>;
			reg = <0x86000 0x1000>;
			reset-names = "phy\0common";
			resets = <0x02 0x4e 0x02 0x4f>;
			status = "ok";
		};

		phy@8e000 {
			#phy-cells = <0x00>;
			clock-names = "pipe_clk";
			clock-output-names = "pcie20_phy1_pipe_clk";
			clocks = <0x02 0x79>;
			compatible = "qcom,ipq8074-qmp-pcie-phy";
			linux,phandle = <0x0c>;
			phandle = <0x0c>;
			reg = <0x8e000 0x1000>;
			reset-names = "phy\0common";
			resets = <0x02 0x52 0x02 0x53>;
			status = "ok";
		};

		pinctrl@1000000 {
			#gpio-cells = <0x02>;
			#interrupt-cells = <0x02>;
			compatible = "qcom,ipq8074-pinctrl";
			gpio-controller;
			interrupt-controller;
			interrupts = <0x00 0xd0 0x04>;
			linux,phandle = <0x0b>;
			phandle = <0x0b>;
			reg = <0x1000000 0x300000>;

			hsuart-pins {
				bias-disable;
				drive-strength = <0x08>;
				function = "blsp2_uart";
				linux,phandle = <0x05>;
				phandle = <0x05>;
				pins = "gpio46\0gpio47\0gpio48\0gpio49";
			};

			i2c-0-pinmux {
				bias-disable;
				drive-strength = <0x08>;
				function = "blsp1_i2c";
				linux,phandle = <0x07>;
				phandle = <0x07>;
				pins = "gpio42\0gpio43";
			};

			qpic-pins {
				bias-disable;
				drive-strength = <0x08>;
				function = "qpic";
				linux,phandle = <0x09>;
				phandle = <0x09>;
				pins = "gpio1\0gpio3\0gpio4\0gpio5\0gpio6\0gpio7\0gpio8\0gpio10\0gpio11\0gpio12\0gpio13\0gpio14\0gpio15\0gpio16\0gpio17";
			};

			serial4-pinmux {
				bias-disable;
				drive-strength = <0x08>;
				function = "blsp4_uart1";
				linux,phandle = <0x03>;
				phandle = <0x03>;
				pins = "gpio23\0gpio24";
			};

			spi-0-pins {
				bias-disable;
				drive-strength = <0x08>;
				function = "blsp0_spi";
				linux,phandle = <0x06>;
				phandle = <0x06>;
				pins = "gpio38\0gpio39\0gpio40\0gpio41";
			};
		};

		serial@78af000 {
			clock-names = "core\0iface";
			clocks = <0x02 0x22 0x02 0x15>;
			compatible = "qcom,msm-uartdm-v1.4\0qcom,msm-uartdm";
			interrupts = <0x00 0x6b 0x04>;
			linux,phandle = <0x10>;
			phandle = <0x10>;
			reg = <0x78af000 0x200>;
			status = "disabled";
		};

		serial@78b1000 {
			clock-names = "core\0iface";
			clocks = <0x02 0x24 0x02 0x15>;
			compatible = "qcom,msm-uartdm-v1.4\0qcom,msm-uartdm";
			dma-names = "tx\0rx";
			dmas = <0x04 0x04 0x04 0x05>;
			interrupts = <0x00 0x132 0x04>;
			linux,phandle = <0x11>;
			phandle = <0x11>;
			pinctrl-0 = <0x05>;
			pinctrl-names = "default";
			reg = <0x78b1000 0x200>;
			status = "ok";
		};

		serial@78b3000 {
			clock-names = "core\0iface";
			clocks = <0x02 0x26 0x02 0x15>;
			compatible = "qcom,msm-uartdm-v1.4\0qcom,msm-uartdm";
			interrupts = <0x00 0x134 0x04>;
			linux,phandle = <0x0f>;
			phandle = <0x0f>;
			pinctrl-0 = <0x03>;
			pinctrl-names = "default";
			reg = <0x78b3000 0x200>;
			status = "ok";
		};

		spi@78b5000 {
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			clock-names = "core\0iface";
			clocks = <0x02 0x17 0x02 0x15>;
			compatible = "qcom,spi-qup-v2.2.1";
			dma-names = "tx\0rx";
			dmas = <0x04 0x0c 0x04 0x0d>;
			interrupts = <0x00 0x5f 0x04>;
			linux,phandle = <0x12>;
			phandle = <0x12>;
			pinctrl-0 = <0x06>;
			pinctrl-names = "default";
			reg = <0x78b5000 0x600>;
			spi-max-frequency = <0x2faf080>;
			status = "ok";

			m25p80@0 {
				#address-cells = <0x01>;
				#size-cells = <0x01>;
				compatible = "jedec,spi-nor";
				reg = <0x00>;
				spi-max-frequency = <0x2faf080>;
			};
		};

		timer {
			compatible = "arm,armv8-timer";
			interrupts = <0x01 0x02 0xf08 0x01 0x03 0xf08 0x01 0x04 0xf08 0x01 0x01 0xf08>;
		};

		timer@b120000 {
			#address-cells = <0x01>;
			#size-cells = <0x01>;
			clock-frequency = <0x124f800>;
			compatible = "arm,armv7-timer-mem";
			ranges;
			reg = <0xb120000 0x1000>;

			frame@b120000 {
				frame-number = <0x00>;
				interrupts = <0x00 0x08 0x04 0x00 0x07 0x04>;
				reg = <0xb121000 0x1000 0xb122000 0x1000>;
			};

			frame@b123000 {
				frame-number = <0x01>;
				interrupts = <0x00 0x09 0x04>;
				reg = <0xb123000 0x1000>;
				status = "disabled";
			};

			frame@b124000 {
				frame-number = <0x02>;
				interrupts = <0x00 0x0a 0x04>;
				reg = <0xb124000 0x1000>;
				status = "disabled";
			};

			frame@b125000 {
				frame-number = <0x03>;
				interrupts = <0x00 0x0b 0x04>;
				reg = <0xb125000 0x1000>;
				status = "disabled";
			};

			frame@b126000 {
				frame-number = <0x04>;
				interrupts = <0x00 0x0c 0x04>;
				reg = <0xb126000 0x1000>;
				status = "disabled";
			};

			frame@b127000 {
				frame-number = <0x05>;
				interrupts = <0x00 0x0d 0x04>;
				reg = <0xb127000 0x1000>;
				status = "disabled";
			};

			frame@b128000 {
				frame-number = <0x06>;
				interrupts = <0x00 0x0e 0x04>;
				reg = <0xb128000 0x1000>;
				status = "disabled";
			};
		};
	};
};
